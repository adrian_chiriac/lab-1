
;-------------------------------------------------------------------------------
; MSP430 Assembler Code Template for use with TI Code Composer Studio
;
;
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file

;-------------------------------------------------------------
; Lab 1 - Simple MSP430 calculator with Code Composer Studio
; C2C Adrian Chiriac, ROAF / 02 Sept 2015 / 18 Sept 2015
;
; This program is a creating a simple calculator in assembly
;-------------------------------------------------------------
;
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file

;-------------------------------------------------------------------------------
;Memory space at 0x0200 for results and intermediates:
					.data
userResults:		.space	50 		; enough bytes in RAM for what we are doing

            .text                           ; Assemble into program memory
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section
            .retainrefs                     ; Additionally retain any sections
                                            ; that have references to current
                                            ; section
;The values that we will process
userInput:	.byte	0x22, 0x11, 0x22, 0x22, 0x33, 0x33, 0x08, 0x44, 0x08, 0x22, 0x09, 0x44, 0xff, 0x11, 0xff, 0x44, 0xcc, 0x33, 0x02, 0x33, 0x00, 0x44, 0x33, 0x33, 0x08, 0x55
;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer

;-------------------------------------------------------------------------------
                                            ; Main loop here
;-------------------------------------------------------------------------------
; I need the input (ROM)

	;Required functionality
	;	input: 0x11, 0x11, 0x11, 0x11, 0x11, 0x44, 0x22, 0x22, 0x22, 0x11, 0xCC, 0x55
	;	output: 0x22, 0x33, 0x00, 0x00, 0xCC
	;B functionality
	;	input: 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0xDD, 0x44, 0x08, 0x22, 0x09, 0x44, 0xFF, 0x22, 0xFD, 0x55
	;	output:	0x22, 0x33, 0x44, 0xFF, 0x00, 0x00, 0x00, 0x02
	;A functionality
	;	input: 0x22, 0x11, 0x22, 0x22, 0x33, 0x33, 0x08, 0x44, 0x08, 0x22, 0x09, 0x44, 0xff, 0x11, 0xff, 0x44, 0xcc, 0x33, 0x02, 0x33, 0x00, 0x44, 0x33, 0x33, 0x08, 0x55
	;	output: 0x44, 0x11, 0x88, 0x00, 0x00, 0x00, 0xff, 0x00, 0xff, 0x00, 0x00, 0xff
	;Req1 functionality
	;	input: 0x14, 0x11, 0x32, 0x22, 0x08, 0x44, 0x04, 0x11, 0x08, 0x55
	;	output:	0x46 0x3e 0x00 0x0c
	;Req2 functionality
	;	input: 0x22, 0x11, 0x22, 0x22, 0x33, 0x33, 0x08, 0x44, 0x08, 0x22, 0x09, 0x44, 0x22, 0x22, 0x22, 0x11, 0x33, 0x33, 0x00, 0x44, 0x33, 0x33, 0x08, 0x55
	;	output:	0x44	0x11	0x88	0x00	0x00	0x00	0x00	0x33	0x00 0x00	0xFF


;What the progam will be capable of doing and when it should be doing it.
;I'm not planning to type numbers all night so this will help
ADD_OP:				.equ	0x11
SUB_OP:				.equ	0x22
MUL_OP:				.equ	0x33
CLR_OP:				.equ	0x44
END_OP:				.equ	0x55

;Constants
MAX_VAL:			.equ	255
ZERO:				.equ	0

;-------------------------------------------------------------------
;Actual code

;DO NOT FORGET YOU ARE WORKING WITH .b

;Need to keep track of where I am reading/writing
	mov		#userInput,	R8					;ROM pointer
	mov		#userResults,	R9			;RAM pointer

;Get operands
	mov.b	@R8+,		R6		;store first operand and increase ROM pointer

;I need to get the next two chuncks of data (OP_code and 2nd operand)
;Function that gets two chuncks of data (op code and operand)

getData:
	mov.b @R8+,R5 ;got the OP_code
	mov.b @R8+, R7	;second operand

;Big picture for what to do

doOP:
		cmp.b	#ADD_OP,	R5
			jz	doADD
		cmp.b	#SUB_OP,	R5
			jz	doSUB
		cmp.b	#MUL_OP,	R5
			jz	doMUL
		cmp.b	#CLR_OP,	R5
		jz	doCLR
		;this will exit either on command or op code error
		jmp end

doADD:	;self explanatory. Just add them up. I can only go over 255 so I don't need to check for ZERO
		add.b R7,	R6
		;need to check boundries
		jc saveMAX ;writes max value in register
		;store the sum
		jmp saveRAM

doSUB:	;self explanatory. Just substract them up. I can only go below ZERO so I don't need to check for 255
		sub.b R7,	R6
		;need to check boundries
		jn saveZERO ;writes 0 if boundry is exceded
		;store the difference
		jmp saveRAM
doCLR:
		mov.b	R7,			R6					;first operand is now the second operand
		mov.b	#ZERO,		0(R9)				;store 0 to next RAM location
		inc		R9 ;increment RAM pointer
		jmp 	getData

;clear junk and trying to do a O(log n) after christian explained me how it works
doMUL:

	clr R11		;placeholder register
	clr R12		;intermediate result
	clr R13		;final result

;executes after doMUL is started

loopMUL:			;Christian's example 4x5 = 4*2^0 + 4*2^2  ; 5 = 0101 ; the second operand has 1 at placeholder 0 and 2
		add.b R12,R13 			;add intermediate result from bitShiftLoop to cumulative result
		jc saveMAX
		clr R12						;result from each iteration of loopMUL
		tst.b R7
		jz	breakIfZero				;once second operand reaches 0, exit multiplication
		mov.b R11,R14				;copy of placeholder for use in bitShiftLoop
		inc.b R11				;increase for the next loop's place
		clrc
		rrc.b R7		;cut second operand in half
		jnc loopMUL
		add.b R6, R12		;first operand into temporary result register

bitShiftLoop:		;double first operand the number of times equal to the placeholder value, store to R12
	tst	R14
	jz 		loopMUL
	clrc
	rlc.b	R12
	jc		saveMAX
	dec.b	R14
	jmp		bitShiftLoop

breakIfZero:
	mov.b	R13,		R6					;put final product into R6
	jmp		saveRAM

saveMAX: ;writes 255 in RAM
		mov.b	#MAX_VAL, R6
		jmp saveRAM

saveZERO: ;writes 0 in RAM
		clr R6
		jmp saveRAM


saveRAM: ;writes the values of R6 in RAM
		mov.b R6,	0(R9);save the value.
		inc R9		;don't lose the pointer
		jmp	getData	;Live. Die. Repeat!

end:
	jmp		end					;to make sure it can be stoped. infinite loop

;-------------------------------------------------------------------------------
;           Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect 	.stack

;-------------------------------------------------------------------------------
;           Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
