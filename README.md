Lab 1 - A simple calculator #

## Objective ##

Write a program that interprets a series of operands and operations and stores the results - an assembly language calculator!

# PreLab #

I read several times thorough the instructions and when a got an idea of what is asked I was able to write the pseudocode.
Having a good understanding of what needs to be done made the pseudocode easy to write which in turn made the actual code easier to write.

The pseudocode it's pretty self explanatory and it does not need further explanations.

![20150908_113047.jpg](https://bitbucket.org/repo/G4GqLy/images/731730861-20150908_113047.jpg)# 

# Lab #

## Debugging ##

The debugging processes was not entertaining. The few spelling errors were easy to solve since we have a syntax checker. The problem was the placement in program of the input. I had to get EI in order to figure out why the watchdog was going crazy and my input wasn't read properly. It worked in the end.

A bug that was driving me nuts was in the subtraction function. Since I copied the code from addition to subtraction and only change few parts I didn't notice that the **jc** instruction was not changed to **jn**. That caused my code to not work properly but after reading it carefully I noticed the mistake.    

## Testing methodology / results ##

The **Basic Functionality** was fairly easy to implement. After implementing that, adding the ** B Functionality ** was even easier.

When tested the results were not as expected.

** Basic Functionality: Addition, subtraction, and clear **

This test verifies if addition, subtraction, and clear operations work properly.

Input: 0x11, 0x11, 0x11, 0x11, 0x11, 0x44, 0x22, 0x22, 0x22, 0x11, 0xCC, 0x55

Output: 0x22, 0x33, 0x00, 0x00, 0xCC

Running this test outputted different results than what expected. Stepping through it made me realize that sub function is not working properly and had to be fixed. After patching it up the test was successful.

** B Functionality: Store minimum and maximum values **

This test verifies that the boundaries of our numerical system are not exceeded.
  
input: 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0xDD, 0x44, 0x08, 0x22, 0x09, 0x44, 0xFF, 0x22, 0xFD, 0x55

output:	0x22, 0x33, 0x44, 0xFF, 0x00, 0x00, 0x00, 0x02

B Functionality test was run after the Basic Functionality test was performed. The results were as expected.

** A Functionality: Multiplication in O(log n) **

input: 0x22, 0x11, 0x22, 0x22, 0x33, 0x33, 0x08, 0x44, 0x08, 0x22, 0x09, 0x44, 0xff, 0x11, 0xff, 0x44, 0xcc, 0x33, 0x02, 0x33, 0x00, 0x44, 0x33, 0x33, 0x08, 0x55

output: 0x44, 0x11, 0x88, 0x00, 0x00, 0x00, 0xff, 0x00, 0xff, 0x00, 0x00, 0xff

The test was performed successfully.

**Req1 functionality**

This test was performed to see how the program will perform if invalid Op codes were given 

input: 0x14, 0x11, 0x32, 0x22, 0x08, 0x44, 0x04, **0x08**, 0x55

output:	0x46 0x3e 0x00

**Req2 functionality**

This test does not test anything in particular. It's just random numbers to check if something breaks.

input: 0x22, 0x11, 0x22, 0x22, 0x33, 0x33, 0x08, 0x44, 0x08, 0x22, 0x09, 0x44, 0x22, 0x22, 0x22, 0x11, 0x33, 0x33, 0x00, 0x44, 0x33, 0x33, 0x08, 0x55

output:	0x44	0x11	0x88	0x00	0x00	0x00	0x00	0x33	0x00 0x00	0xFF

Nothing broke.

## Observations and Conclusions ##

You can do some cool stuff with assembly. This Lab made me a little bit more interested in learning Assembly (for CNS purposes)

## Documentation ##

Capt Trimble help me figure out what was the problem when reading the input

C1C Arnold walked me through the multiplication operation and provided an example in order to be able to code it

http://www.tutorialspoint.com/assembly_programming/assembly_arithmetic_instructions.htm for assembly instruction